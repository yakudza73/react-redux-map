import { combineReducers } from 'redux'
import { pageReducer } from './page'
import { userReducer } from './user'
import { map } from "./map"

export const rootReducer = combineReducers({
  page: pageReducer,
  user: userReducer,
  map
})
