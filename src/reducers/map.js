
const initialState = {
  address: '',
  points:[/*{text:"москва", id: Number( Math.random().toString().slice(3) )}*/]
}

export function map(state = initialState, action) {
  switch (action.type) {
      case "SET_ADDRESS_ACTION":
          return { ...state, address: action.payload }

      case "SET_POINT":
        const point = {
          text: action.payload,
          id: Number( Math.random().toString().slice(3) )
        }
        const newPointsList = [ ...state.points]
        newPointsList.push(point)
        return { ...state, points: [ ...newPointsList] }

      case "DELETE_POINT":
        const newPoints = state.points.filter(v => v.id !== action.payload)
        return { ...state, points: [...newPoints] }

      case "CHANGE_POINT_LIST":
        console.log(action.payload, 'CHANGE_POINT_LIST');
        
          return { ...state, points: [...action.payload ] }

      default:
          return state
  }
}