
export function setAddressAction(address) {
  return dispatch => {
      dispatch({
          type: "SET_ADDRESS_ACTION",
          payload: address,
      })
  }
}

export function setPoint(point) {
  return dispatch => {
      dispatch({
          type: "SET_POINT",
          payload: point,
      })
  }
} 

export function deletePoint(pointId) {
  return dispatch => {
      dispatch({
          type: "DELETE_POINT",
          payload: pointId,
      })
  }
} 

export function changePointList(pointList) {
  return dispatch => {
      dispatch({
          type: "CHANGE_POINT_LIST",
          payload: pointList,
      })
  }
}