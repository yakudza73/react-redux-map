import React, { Component } from 'react'
import MapPage from './MapPage' // изменили импорт

class App extends Component {
    render() {
        return (
            <div className="app">
                <MapPage />
            </div>
        )
    }
}

export default App
