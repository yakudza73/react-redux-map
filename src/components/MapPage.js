import React from 'react';
import { makeStyles, createStyles, Theme, withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import MapComponent from './MapComponent'
import Search from "./Search"
import AddressList from "./AddressList"
import { useSelector } from 'react-redux'

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      background: `url(${require('../assets/img/image.jpg')})`,
      padding: '50px',
      height: '100vh',
      '&:after': {
        content: '',
        display: 'block',
        height: '100%',
        width: '100%',
        background: 'black'
      }
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    title: {
      textAlign: 'center',
      color: 'white'
    }
  }),
);

export default function MapPage() {
  const points = useSelector(store => store.map.points)
  console.log(points, 'store fr comp');
  const classes = useStyles();

    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
          <Typography variant="h4" className={classes.title}>
            Вводите адрес точки чтобы построить нужный маршрут
          </Typography>
            
          </Grid>
          <Grid item sm={12} md={6}>
            <Paper className={classes.paper}>
              <Search />
              <AddressList />
            </Paper>
            
          </Grid>
          <Grid item sm={12} md={6}>
            <Paper className={classes.paper}>карта</Paper>
            <MapComponent />
          </Grid>
        </Grid>
      </div>
    );
}