
import React from 'react'
import SearchIcon from '@material-ui/icons/Search';
import { fade, makeStyles, createStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import { useSelector, useDispatch } from 'react-redux'
import { setAddressAction, setPoint } from '../actions/map';
import { useState } from 'react';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) =>
  createStyles({
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      margin: 0,
      padiingBottom: theme.spacing(2),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        margin: `${theme.spacing(3)}px 0 0`,
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer'
    },
    inputRoot: {
      color: 'inherit',
      display: 'block'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
    searchButton: {
      position: 'absolute',
      top: 0,
      right: 0
    }
  })
);

const Search = () => {
  const dispatch = useDispatch();
  const [address, setAddress] = useState('');
  const classes = useStyles();

  const submit = (e) => {
    e.preventDefault()
    dispatch(setPoint(address))
    setAddress("")
    console.log('submit');
  }
  return (

    <form className={classes.search} onSubmit={ submit }>
      <div className={classes.searchIcon} onClick={ submit }>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Введите адрес…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
        value={address}
        onChange={(e) => setAddress( e.target.value )}
      />
      <Button variant="contained" color="secondary" classes={{
          root: classes.searchButton,
        }} onClick={ submit }>
        Сохранить
      </Button>
    </form>
  )
}

export default Search