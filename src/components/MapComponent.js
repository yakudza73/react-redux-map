import React, { Component } from 'react'
import { connect } from 'react-redux'

class MapComponent extends Component{
    constructor(props) {
        super(props)
        this.myMap = {}
        this.multiRoute = {}
        this.points = this.props.points
    }

    link = `https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=9f5ac9c7-6354-4aea-a9eb-37e3a2c9a5d9`
    createMap = () => {
        const YMAP_API_KEY = "8c65445a-e25a-4894-a123-eb4bf0fb3598"
        const mapScript =  document.createElement('script')

        mapScript.setAttribute('src', this.link);
        mapScript.setAttribute('async', '');
        mapScript.setAttribute('defer', '');
        document.body.appendChild(mapScript);
        mapScript.onload = () => {
            window.ymaps.ready(() => {
                this.init()
            });
        };
    }

    init = () => {
        const multiRoute = new window.ymaps.multiRouter.MultiRoute({
            referencePoints: this.pointsNameList.length > 0 ? this.pointsNameList : []
        }, {
            editorMidPointsType: "via",
            draggable: true
        });
        this.setState({multiRoute})
        
        const myMap = new window.ymaps.Map("map", {
            center: [56.399625, 36.71120],
            zoom: 7,
        })
        myMap.geoObjects.add(multiRoute);
        this.setState({myMap})
    }

    getCoordinates = (queryString) => window.ymaps.geocode(queryString, {
        results: 1
    }).then((res) => {
            const firstGeoObject = res.geoObjects.get(0)
            const coords = firstGeoObject.geometry.getCoordinates()
            return coords
    })

    get pointsNameList() {
        return this.points.map(v => v.text)
    }

    componentDidMount(){
        const isLoadedScript = document.querySelector(`script[src*="${this.link}"]`)

        if(!isLoadedScript) {
            this.createMap()
        } else {
            
        }
    }

    async componentWillReceiveProps(next){
        if(next){
            const pointsToStr = next.points.map(v => v.text)
            this.state.multiRoute.model.setReferencePoints(pointsToStr)

            const lastPoint = next.points[next.points.length - 1]
            const lastPointCoord = await this.getCoordinates(lastPoint.text )
            this.state.myMap.setCenter(lastPointCoord)
        }
    }

    render() {
        return (
                <React.Fragment>
                    <div id="map" style={{ height: 500 }}></div>
                </React.Fragment>
        )
    }
    
}

const mapStateToProps = store => {
    return {
        points: store.map.points
    }
}

const mapDispatchToProps = dispatch => {
    return {
        
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MapComponent)