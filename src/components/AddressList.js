import * as React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import { useSelector, useDispatch } from 'react-redux'
import { deletePoint, changePointList } from "../actions/map"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";




const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      maxWidth: 752,
    },
    demo: {
      backgroundColor: theme.palette.background.paper,
    },
    title: {
      margin: `${theme.spacing(4, 0, 2)} 0 0`,
      textAlign: "center"
    },
  }),
);

//перестановка
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightblue" : "initial"
});

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",

  // change background colour if dragging
  background: isDragging ? "lightgreen" : "initial",

  // styles we need to apply on draggables
  ...draggableStyle
});


  

export default function AddressList() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [dense, setDense] = React.useState(false);
  const points = useSelector(store => store.map.points)

  const onDragEnd = (result) => {
    // конец перемещения
    if (!result.destination) {
      return;
    }
  
    const items = reorder(
      points,
      result.source.index,
      result.destination.index
    );
    
    dispatch(changePointList(items))
  }

  return (
    <div className={classes.root}>
      <Typography variant="h6" className={classes.title}>
        Маршрутный лист
          </Typography>
      {points.length > 0 ?
        <div className={classes.demo}>
          <List dense={dense}>

            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                  <div
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >

                    {points.map((v, index) => (
                      <Draggable key={v.id} draggableId={v.id.toString()} index={index}>
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={getItemStyle(
                              snapshot.isDragging,
                              provided.draggableProps.style
                            )}
                          >

                            <ListItem key={v.id}>
                              <ListItemText
                                primary={v.text}
                              />
                              <ListItemSecondaryAction onClick={() => dispatch(deletePoint(v.id))}>
                                <IconButton edge="end" aria-label="delete">
                                  <DeleteIcon />
                                </IconButton>
                              </ListItemSecondaryAction>
                            </ListItem>

                          </div>
                        )}
                      </Draggable>

                    ))}

                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </List>
        </div>
        :
        <h1>начните вводить адрес и здесь появится список точек</h1>
      }

    </div>
  );
}