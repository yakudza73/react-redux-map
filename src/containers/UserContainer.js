import React, { Component } from 'react'
import { connect } from 'react-redux'
import { User } from '../components/User'
import {handleLogin} from "../actions/UserActions";

class UserContainer extends Component {
    render() {
        const { user, handleLoginAction } = this.props
        return (
            <div className="app">
                <User
                    name={user.name}
                    isFetching={user.isFetching}
                    error={user.error}
                    handleLogin={handleLoginAction}
                />
            </div>
        )
    }
}

const mapStateToProps = store => {
    return {
        user: store.user,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // "приклеили" в this.props.handleLoginAction функцию, которая умеет диспатчить handleLogin
        handleLoginAction: () => dispatch(handleLogin()),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserContainer)
